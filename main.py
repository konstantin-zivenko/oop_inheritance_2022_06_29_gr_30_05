import hr
import employees
import productivity
import contacts


manager = employees.Manager(1, "Semen Semenko", 20000)
manager.address = contacts.Address(
    "b.6, Sportivnajs str.",
    "Odessa",
    "Ukraina",
    "65000",
)
secretary = employees.Secretary(4, "Maria Smith", 15000)
factory_worker = employees.FactoryWorker(2, "Taras Gulchuk", 40, 500)
sales_guy = employees.SalesPerson(3, "Oksana Rabko", 10000, 8000)
temporary_secretary = employees.TemporarySecretary(5, "Svitlana Sokol", 40, 300)
temporary_secretary.address = contacts.Address(
    "b.10, Budivelnikiv str.",
    "Irpin",
    "Ukraina",
    "08200",
)


employees_list = [
    manager,
    secretary,
    factory_worker,
    sales_guy,
    temporary_secretary
]

payroll_system = hr.PayrollSystem()
payroll_system.calculate_payroll(employees_list)

productivity_system = productivity.ProductivitySystem()
productivity_system.track(employees_list, 40)