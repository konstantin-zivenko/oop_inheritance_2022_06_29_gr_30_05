class Address:
    def __init__(self, street, city, state, zipcode, street_2=""):
        self.street = street
        self.street_2 = street_2
        self.city = city
        self.state = state
        self.zipcode = zipcode

    def __str__(self):
        lines = [self.street]
        if self.street_2:
            lines.append(self.street_2)
        lines.append(f"{self.city}, {self.state}, {self.zipcode}")
        return "\n".join(lines)
